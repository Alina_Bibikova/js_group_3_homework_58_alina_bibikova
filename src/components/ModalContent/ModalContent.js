import React, {Fragment} from 'react';

const ModalContent = props => {
    return (
        <Fragment>
            <div>
                <h1>Your Title</h1>

                <button className='closeButton'
                        onClick={props.showAlert}
                >
                    X
                </button>

                <p>This is modal content</p>
                {props.children}
            </div>
        </Fragment>
    );
};

export default ModalContent;