import React from 'react';
import './ButtonContent.css';

const ButtonContent = props => {
    return (
        <div>
            <button className='ContentButton'
                    onClick={props.purchase}
            >
                Show Title
            </button>
        </div>
    );
};

export default ButtonContent;