import React, {Fragment} from 'react';
import Button from "../UI/Buttons/Button/Button";
import './AlertContent.css';

const AlertContent = props => {
    const block = props.showAlert ? 'block' : 'none';
    return (
        <Fragment>
                <div className='AlertContent' style={{display: block}}>
                    <p>This is Alert Content</p>
                    <Button onClick={props.purchaseCancel}><span style={{backgroundColor: '#b02223',
                        boxShadow: "0 0 10px rgba(0,0,0,0.5)"}}>
                        cancel</span>
                    </Button>
                    <Button onClick={props.purchaseContinue}><span style={{backgroundColor: '#21a5cc',
                        boxShadow: "0 0 10px rgba(0,0,0,0.5)"}}>
                        continue</span>
                    </Button>
                </div>
        </Fragment>
    );
};

export default AlertContent;