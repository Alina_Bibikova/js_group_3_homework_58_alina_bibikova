import React, { Component, Fragment } from 'react';
import Modal from "../../components/UI/Modal/Modal";
import ButtonContent from "../../components/UI/Buttons/ButtonContent/ButtonContent";
import ModalContent from "../../components/ModalContent/ModalContent";
import AlertContent from "../../components/AlertContent/AlertContent";


class GeneralContent extends Component {
    state = {
        purchasing: false,
        someHandler: false,
        showAlert: false
    };

    purchase = () => {
        this.setState({purchasing: true})
    };

    purchaseCancel = () => {
        this.setState({showAlert: false, purchasing: false})
    };

    purchaseContinue = () => {
        alert('This is a success type alert?');
        this.setState({purchasing: false, showAlert: false});
    };

    showAlert = () => {
        this.setState({
            showAlert: true
        })
    };

    render() {
        return (
            <Fragment>
                <Modal
                    show={this.state.purchasing}
                    close={this.purchaseCancel}
                >
                    <ModalContent
                        showAlert={() => this.showAlert()}
                    />
                    <AlertContent
                        showAlert={this.state.showAlert}
                        purchaseCancel={() => this.purchaseCancel()}
                        purchaseContinue={() => this.purchaseContinue()}
                    />
                </Modal>
                <ButtonContent
                    purchase={(() => this.purchase())}
                />
            </Fragment>
        );
    }
}

export default GeneralContent;
