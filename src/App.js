import React, { Component } from 'react';
import './App.css';
import GeneralContent from "./containers/GeneralContent/GeneralContent";

class App extends Component {
  render() {
    return (
      <div className="container">
        <GeneralContent />
      </div>
    );
  }
}

export default App;
